<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('task', 'TaskController@index');
$router->post('task', 'TaskController@add');
$router->patch('task/{taskId}', 'TaskController@update');
$router->delete('task/{taskId}', 'TaskController@delete');
?>