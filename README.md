# Lumen PHP Framework

## Getting started

## Pré-requis
PHP 8.2 https://www.php.net/manual/en/install.php

Extension mongodb (https://www.php.net/manual/en/mongodb.installation.pecl.php)

## Installation
Cloner le projet

Se placer à la racine du projet

`composer install --ignore-platform-reqs`

Créer une base de données test avec une collection tasks dans Mongo Atlas, autoriser l'ip du serveur et récupérer la clé de connexion, la placer dans .env https://www.mongodb.com/cloud/atlas/register

Servir le projet à l'aide d'apache ou nginx

## Usage
GET http://91.175.246.3/task

PATCH http://91.175.246.3/task/{id} 
body: Task (date: String yyyy-mm-dd, name: String, description: String)

DELETE http://91.175.246.3/task/{id}

## Support
Florian Wattier
fwattier@live.fr

## Roadmap
Dans le futur, ajout de routes (import, export, statistiques, ...)

Ajout d'un système d'authentification, de tests automatisés

Un Swagger

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/lumen-framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/lumen-framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/lumen)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
