<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(): JsonResponse 
    {
        $tasks = Task::all();
        return response()->json($tasks);
    }

    public function add(Request $request): JsonResponse 
    {
        $task = new task;
 
        $task->date = new \MongoDB\BSON\UTCDateTime(strtotime($request->input("date")) * 1000);
        $task->name = $request->name;
        $task->description = $request->description;
 
        $task->save();
        
        return response()->json($task);
    }

    public function update(Request $request, $taskId): JsonResponse 
    {
        $task = Task::find($taskId);

        $task->date = new \MongoDB\BSON\UTCDateTime(strtotime($request->input("date")) * 1000);
        $task->name = $request->name;
        $task->description = $request->description;
        
        $task->save();
        return response()->json($task);
    }

    public function delete($taskId): JsonResponse 
    {
        $task = Task::find($taskId);
        $task->delete();
        return response()->json("success");
    }
}
